"use strict";
var APP = {};
(function(){
/* CONNECTION STRINGS */
    var randomUserQuery = "https://randomuser.me/api?results=";

//PART-2
    APP.usersArray;

//PART-3, in:users_table, out:save to usersArray
function generateUsersArray(users){
    return users.map(function(user){
        return {
            userData: user,
            state: "visible",
            tr: generateUserRow(user)
        };
    });
}

//PART-4,create TR in usersArray
function generateUserRow(user){
        var tr = document.createElement("tr"),
            tdFirstName = document.createElement("td"),
            tdLastName = document.createElement("td"),
            tdEmail = document.createElement("td");

        tdFirstName.innerText = user.name.first;
        //hideUserRecord(tdFirstName);
        tdLastName.innerText = user.name.last;
        tdEmail.innerText = user.email;
        tr.appendChild(tdFirstName);
        tr.appendChild(tdLastName);
        tr.appendChild(tdEmail);

        return tr;
}

//PART-5, adding rows to table
function initView(targetTable, rows){
    rows.map(function(row){
    return targetTable.appendChild(row.tr);
    });
}

//PART-6, connect to server and initialize
APP.loadUsers = function(count) {
    var count = count||10;
    var oReq = new XMLHttpRequest();
    oReq.open("GET", randomUserQuery + count);
    oReq.onreadystatechange = function() {
        if (oReq.readyState === 4) {
            var users = JSON.parse(oReq.responseText).results;
            APP.usersArray = generateUsersArray(users);  //PART-1

            initView(document.getElementById("randomUsers"), APP.usersArray);
        }
    };
    oReq.send();
}

/* PART-II - HIDE USERSs */
function hideUserRecord(ev) {
    ev.target.toggle('hiddenStyle');
}


/* ADD EVENT LISTENERS */
document.getElementsByClassName('btn')[0]
    .addEventListener('click', btnOnClick);
document.getElementById("filterQueryInput")
    .addEventListener('keyup', function(){
        time && clearTimeout(time);
        time = setTimeout(filterUsersByName, 2000);
    });
function btnOnClick(ev) {
        ev.target.classList.toggle('whenClickOnkStyle');
    };

var time;
function filterUsersByName() {
    console.log("Test value: ");
    //ev.target.classList
    APP.usersArray = changeVisibility(APP.usersArray);
    initView(document.getElementById("randomUsers"), APP.usersArray);
}

})();

/* ADD FILTER */
function changeVisibility(users) {
    return users.map(function(user){
        var surname = user.userData.name.last;
        var filter = document.getElementById("filterQueryInput").innerText;
        var lookup = surname.indexOf(filter);
        if(lookup === -1 ) {
            return {
            state: "hidden"
            };
        } else if(lookup > -1) { 
            return {
            state: "visible"
            };
        }
    });
}

APP.loadUsers();